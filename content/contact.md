+++
title = "Contact"
aliases = ["about-us", "about-hugo", "contact"]
author = "Dhifaf Ikhsan Sabil"
+++

- [github.com/diszyfoxx](https://github.com/diszyfoxx)
- [kaggle.com/dhifafikhsansabil](https://www.kaggle.com/dhifafikhsansabil)
- [instagram.com/dhifaf__](https://www.instagram.com/dhifaf__)
- [linkedin.com/in/dhifaf-ikhsan-sabil](https://www.linkedin.com/in/dhifaf-ikhsan-sabil/)
