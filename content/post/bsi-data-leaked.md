+++
author = "Dhifaf Ikhsan Sabil"
title = "Bank Syariah Indonesia Data Leaked Tweets: EDA and Sentiment Analysis"
date = "2023-08-04"
description = "Lorem Ipsum Dolor Si Amet"
tags = [
    "Exploratory Data Analysis",
    "Sentiment Analysis",
    "Data Leaked",
]
weight = 10
+++
## Introduction
In this project, I will analyse tweets related to the Bank Syariah Indonesia data leak case when the news spreads to social media, especially Twitter. If you want to focus more on the code, you can check my repository below:
- [My Kaggle Notebook](https://www.kaggle.com/code/dhifafikhsansabil/bsi-data-leaked-tweets-eda-and-sentiment-analysis/notebook)
- [The dataset I'm using](https://www.kaggle.com/datasets/bashirhanafi/bank-bsi-tweets-sentiment-24k-tweets)

## Business Understanding
The LockBit ransomware group released 1.5 terabytes of personal and financial information stolen from Bank Syariah Indonesia after ransom negotiations broke down. The records include the personal and financial information of 15 million customers and employees. Bank Indonesia, the country's central bank, restored its real-time gross settlement, national clearing system, and Bank Indonesia Fast Payment services. 

BSI President and CEO Hery Gunardi said ATMs and bank branch services were restored, and the bank was carrying out capacity building to restore core banking and critical channels. LockBit claimed the bank had lied to customers and partners, and the ransomware group published details of conversations with bank representatives. Indonesian Vice President Ma'ruf Amin called the incident a bad experience and called for the bank to improve its technology to prevent future attacks.

I will perform a sentiment analysis of public opinion tweeted on Twitter about this incident. The results of this sentiment analysis may be able to provide an overview of the level of customer trust and loyalty at BSI.

## Data Understanding
Twitter is a social media platform that allows users to share opinions and opinions from various backgrounds. It plays a significant role in Natural Language Processing (NLP) with vast textual data, language diversity, and real-time trends. In response to the BSI Ransomware incident, 24,000 tweets were generated between May 8 and 18, showcasing the widespread attention and concern among the Twitter community. This highlights Twitter's role as a real-time source of news and public discourse. 

The dataset was created with [snscrape](https://github.com/JustAnotherArchivist/snscrape) by scraping tweets about this incident on Twitter. It contains the following 15 columns:
1. URL: This column contains the URL or link to the specific tweet on the Twitter platform.
2. Date: This column indicates the date and time when the tweet was posted.
3. Tweet: This column contains the text of the tweet itself. It includes the message or content posted by the Twitter user.
4. ID: This column represents the unique identifier assigned to each tweet by Twitter.
5. Username: This column displays the username or handle of the Twitter user who posted the tweet.
6. Replies: This column indicates the number of replies or responses that the tweet has received from other Twitter users.
7. Retweets: This column displays the number of times the tweet has been retweeted by other Twitter users.
8. Likes: This column shows the number of likes or favorites that the tweet has received from other Twitter users.
9. Quotes: This column indicates the number of times the tweet has been quoted by other Twitter users.
10. conversationID: This column represents the unique identifier for the conversation thread to which the tweet belongs.
11. Language: This column specifies the language in which the tweet is written.
12. Links: This column contains any URLs or hyperlinks shared within the tweet.
13. Media: This column indicates the presence of media attachments, such as images, videos, or GIFs, within the tweet.
14. Retweeted Tweet: This column displays the ID or reference to the original tweet that has been retweeted.
15. Bookmarks: This column may indicate the number of times the tweet has been bookmarked by other Twitter users.

## Data Preparation

#### Installing and importing necessary library

```python
# Libraries
!pip install pyspellchecker
!pip install scattertext
!pip install nltk
!pip install -U kaleido

# Import Data Preprocessing and Wrangling libraries
import re
from tqdm.notebook import tqdm
import pandas as pd 
import numpy as np
from datetime import datetime
import dateutil.parser

# Import NLP Libraries
import nltk
from spellchecker import SpellChecker
from nltk.sentiment.vader import SentimentIntensityAnalyzer as SIA

# Import Visualization Libraries
import plotly.offline as pyo 
import plotly.express as px
import plotly.io as pio
import plotly.graph_objects as go
import matplotlib.pyplot as plt
from plotly.subplots import make_subplots
import seaborn as sns 
import scattertext as st
from IPython.display import IFrame
from wordcloud import WordCloud, ImageColorGenerator
import matplotlib.pyplot as plt
from nltk.corpus import stopwords
import random 

# Downloading periphrals
nltk.download('vader_lexicon')
nltk.download('stopwords')

import warnings
warnings.filterwarnings('ignore')
```

#### Initialization
- Using seaborn's darkgrid format as a default visualization style
- Utilizing SentimentIntensityAnalyzer to determine the user sentiments for every tweet (Positive, Neutral, and Negative)
- The SpellChecker library will be used to fix misspelled words
- Plotly will be utilized to view interactive graphs
```python
df_list = []
df = pd.read_csv('/kaggle/input/bank-bsi-tweets-sentiment-24k-tweets/bsi_24k_tweets.csv')
df_list.append(df)

# Intializate our tools 
sns.set_style('darkgrid')

# For sentiment analysis 
sia = SIA() 

# To identify misspelled words
spell = SpellChecker() 

# To display plotly graphs 
pyo.init_notebook_mode() 

# Storing csv dataset into a datframe
df = pd.concat(df_list)
```

#### Data Processing
- Fixing the formatting of the date column
- Using regular expressions to fix the structure of the text and remove unnecessary ASCII symbols
- Create a copy of the original dataset and then create two new columns in the copied dataset called original_tweet and datetime. The original_tweet column contains the original tweet text, while the datetime column contains the date and time of the tweet as a datetime object
- Dropping the date and time columns from the dataset
- Transforming the datetime column using the apply method and the lambda function. This converts the datetime column from a string to a datetime object using the specified format string.
- Doing a boolean mask using the apply method and the lambda function to identify retweets. This is done by checking if the string RT @ is present in the tweet text.
- Converting all the text to lowercase, removing Twitter handles (usernames starting with @), removing hashtags, removing URLs, removing special characters, and removing all single characters. Finally, multiple spaces are substituted with a single space.
```python
data = df.copy()
data['original_tweet'] = df['Tweet']
data['datetime'] = data['Date']
data['datetime'] = data.datetime.apply(lambda x: dateutil.parser.parse(x))
rt_mask = data.Tweet.apply(lambda x: "RT @" in x)

# standard tweet preprocessing 
data.Tweet = data.Tweet.str.lower()
#Remove twitter handlers
data.Tweet = data.Tweet.apply(lambda x:re.sub('@[^\s]+','',x))
#remove hashtags
data.Tweet = data.Tweet.apply(lambda x:re.sub(r'\B#\S+','',x))
# Remove URLS
data.Tweet = data.Tweet.apply(lambda x:re.sub(r"http\S+", "", x))
# Remove all the special characters
data.Tweet = data.Tweet.apply(lambda x:' '.join(re.findall(r'\w+', x)))
#remove all single characters
data.Tweet = data.Tweet.apply(lambda x:re.sub(r'\s+[a-zA-Z]\s+', '', x))
# Substituting multiple spaces with single space
data.Tweet = data.Tweet.apply(lambda x:re.sub(r'\s+', ' ', x, flags=re.I))

# convert the 'date' column to datetime format and remove the timezone information
data['datetime'] = pd.to_datetime(data['datetime']).dt.tz_localize(None)
```

#### Feature Extraction
Initialized libraries above to fix spelling mistakes and apply Sentiment analysis to the individual tweets. The datetime column will also be feature engineered into different types so that it could be used for further visualizations later on.

```python
def label_sentiment(x:float):
    if x < -0.05 : return 'negative'
    if x > 0.35 : return 'positive'
    return 'neutral'

# Feature Extraction
data['words'] = data.Tweet.apply(lambda x:re.findall(r'\w+', x ))
data['errors'] = data.words.apply(spell.unknown)
data['errors_count'] = data.errors.apply(len)
data['words_count'] = data.words.apply(len)
data['sentence_length'] = data.Tweet.apply(len)
data['hour'] = data.datetime.apply(lambda x: x.hour)
data['date'] = data.datetime.apply(lambda x: x.date())
data['month'] = data.datetime.apply(lambda x: x.month)
data['year'] = data.datetime.apply(lambda x: x.year)


# Extract Sentiment Values for each tweet 
data['sentiment'] = [sia.polarity_scores(x)['compound'] for x in tqdm(data['Tweet'])]
data['overall_sentiment'] = data['sentiment'].apply(label_sentiment);
```

## Exploratory Data Analysis and Visualization
- Performing Exploratory Data Analysis and visualize the data.
- Checking missing values, do statistical analysis and view relationships between different data columns.

```python
# Get the count of non-null values for each column
column_counts = data.count()

# Create the bar chart
fig = go.Figure(go.Bar(
    x=column_counts.index,
    y=column_counts.values
))

# Add axis labels and title
fig.update_layout(
    xaxis_title="Column",
    yaxis_title="Count",
    title="Number of Rows per Column",
    title_font=dict(size=20),
    font=dict(size=14),
    height=600,
    template='plotly_dark'
)

# Add a subtitle and subheader
fig.add_annotation(
    xref='paper', yref='paper',
    x=0.5, y=1.15,
    text='<b>Data Column Count Distribution</b>',
    showarrow=False,
    font=dict(size=24)
)

fig.add_annotation(
    xref='paper', yref='paper',
    x=0.5, y=1.05,
    text='Number of non-null rows per column in the dataset',
    showarrow=False,
    font=dict(size=16)
)

# Show the chart
fig.show()
```
{{< figure src="https://www.kaggleusercontent.com/kf/138873655/eyJhbGciOiJkaXIiLCJlbmMiOiJBMTI4Q0JDLUhTMjU2In0..0oy3zdA0sYlZUsMaDn4BrQ.Ohx-FnE8zKoqPcYTEglbCrEkiuhV1E0WVezwnocfjnGJ7pOg_6zuWws3IZlqb9FUS8gjskR8hjovvoupzxy-giD-CVx9vSFnUZEZ7gm2sg_q3XEIg2adflVVQpXwM8hZyAWxg2z-RyeyuCLOHfTaaxVoPiSTmhcEcuyvvn5u5L76AYxu9qyGZWGu_MfqLneB5uNtAUmndA6GdfpfAB7ePloDMdWxbClffFusaYLGPlR7AaELW7-6Rd4CsiTiW-HsVfIivKKzJ9ZVwylU7Dum_QhQRINorrCJSO9fBz1xQdhbLrli-z5kjfHAPJHR5aYc3b7BK5Flmi8wnAoWvL4Xut8JlFGxnPoFA3lLQZg95zCq8Ho8C849x6BqcxPHHVERliWxanJ6pmqy7bPNahe3EcGZbyOJM8I9IprJnBwsc924mrpG4zvk3EgKM7JsAyJ08oyx49WA1D8ympP5pmlQd0uU6LADOexOZd-ZEmtqH7Vo_-UIf3efAkzg08l4ZotsW9qcyObZO1SdtpedGiMJt4_ZPQOynsElvjAq89o3rTVuLr0v7wO5xcIz92FrrTgRmt7HqEncFpNfeCJ3wPAH1SRMwApsvaMcqWNL1pO_Stiv4HY147JKqSzh50PVzSEQ_ujzcyPJCvi6UWE3qB1Z1SxiHCy034IOD-uQtN72mDxSYwHUcdWseQJliHIzMOq265NdbQYfZOtJDb6ceMsPKg.umj-7gbrmgU2rdTdkEu2jQ/Data%20Column%20Count%20Distribution.png" >}}

```python
# Get the count of missing values for each column
column_na_count = data.isna().sum()

# Create the bar chart
fig = go.Figure(go.Bar(
    x=column_na_count.index,
    y=column_na_count.values,
    marker_color='rgb(253, 128, 93)' # Set custom color for the bars
))

# Add axis labels and title
fig.update_layout(
    xaxis_title="Column",
    yaxis_title="Missing Value Count",
    title="Missing Values per Column",
    font=dict(
        size=14,
        color='white'
    ),
    plot_bgcolor='rgb(17, 17, 17)', # Set dark background color
    paper_bgcolor='rgb(17, 17, 17)', # Set dark background color for the entire plot
)

# Add subheader
fig.add_annotation(
    x=0.5,
    y=1.1,
    xref='paper',
    yref='paper',
    text="<b>Number of missing values per column</b>",
    showarrow=False,
    font=dict(
        size=16,
        color='white'
    )
)

# Show the chart
fig.show()
```

{{< figure src="https://www.kaggleusercontent.com/kf/138873655/eyJhbGciOiJkaXIiLCJlbmMiOiJBMTI4Q0JDLUhTMjU2In0..cU2_PlbtlcWz9hHgSf2gEg.JyU6Y70zQgCcPcfKqZPwEfJssnk0DjpjbDGbsjPMZHTyaApmmCFy2s5-48SBwiwV-4jE6xMmunAlY8I5rFq36lqpv1kqStdbVmAlFzz3G9F1C9IJCiXDT0NxHMKwsFuLOoSfMEShvfStMMC0j5An9jIoMAxw2LLy75z5vyCzZQOKn9N_59Qc6EGH5q5RQftV4LwwBL-nTVt_SlT_19nnyiXfIgGkj9bK2iGwkXnywnwgiz3dCHeHJbM-R6xMndDK3-HDDqEKgtZtly1LTDZtHugw_ohDWHlamyHxnmwZX8yp0-RlCTA0cwi46IPQn7or_IH6upY9hzIGAZlGL0UGvpunY8HJtpTjvhu50lvT31HM_KPKrf2QnRxV881A9kBumSdNvXaPXQG7vFzgcHslLBktTeAMGbwPdKhdIk2GtgpgGnGpuCc3HLH00l5JZGgNUT0jn7jwxDrOWbLEAFWIvCxDHdiOzkU5bRQA_P5CprWoBLJjcdXKXlaGxg6R7YmLXYljoYF0UCHpuDifzoG45WVX5yAyPRmdnKgH3KmUegq5zDsOwAzaJp16lvf6LBPlW-igc2zmV03Yz0yJmJGRNwfcEZTg1cmGpm9f5cqfXYX3y7DLmWk3l9KzDAu6jv7cbPGEyzUBiDgWQmaCgRo5MvXP-hNAvqhKhQ2-WVl9ACP9NQS_4yY61pNxKK9bKENGfHcKT3f0bZ7MEioLMiGJIQ.Cvyxm7kUQ5hksid59B2LEg/Missing%20Values%20per%20Column.png" >}}

```python
def plot_heatmap(data, cmap=sns.color_palette(palette='rocket_r', as_cmap=True), height=3, linewidth=1, title=' ', subtitle=' '):
    sns.set(style = 'whitegrid', rc = {'figure.figsize': (20,height)})

    mask = np.zeros_like(data, dtype=bool)
    mask[np.triu_indices_from(mask)] = True
    mask[np.diag_indices_from(mask)] = False

    g = sns.heatmap(data=data, cmap=cmap,
                    linewidths=linewidth,
                    annot=True,
                    fmt='.2f',
                    mask=mask,
                    cbar_kws=dict(location='right'))

    g.set_xlabel(' \n\n\n\n')
    g.set_ylabel(' \n\n\n\n')

    g.set_xticklabels([tick_label.get_text().title() for tick_label in g.get_xticklabels()])
    g.set_yticklabels([tick_label.get_text().title() for tick_label in g.get_yticklabels()])

    g.set_title(f'\n\n\n\n{title}\n\n'.upper(),
                loc='left',
                fontdict=dict(
                    fontsize=20,
                    fontweight='bold'))

    plt.text(s=f'{subtitle}',
             alpha=0.5,
             x=0,
             y=-0.6,
             horizontalalignment='left',
             verticalalignment='top',
             fontsize=16)

    plt.text(s=' ',
             x=1.2,
             y=1,
             transform=g.transAxes)
    plt.savefig('corelation.png')
    return g

plot_heatmap(
    height=15,
    
    data=data.drop(['Retweeted Tweet', 'Language', 'year', 'month'], axis=1).corr(),
    title='Dataset Correlation Overview',
    subtitle='Method of Correlation: Pearson Correlation Coefficient'
);
```

{{< figure src="https://www.kaggleusercontent.com/kf/138873655/eyJhbGciOiJkaXIiLCJlbmMiOiJBMTI4Q0JDLUhTMjU2In0..J2oybqb-jIt4YhdoSEDixQ.tGnL4L6kjpL3_I4R3PnIQyUEnBVjM9UNznNYUUHmZ42yUddgEaR8f7oQZUYtXglW12594E2imHLwk-wzG7hMIcn5DCVTI4IaDlImXK9hDXTsEbXMpk1jf-43VDGr5eamlzFdqM7Vpaeg8Qa34jRlewcrlsk8WheDRaToARxQfFLsDyY9qB2Lfmbbt56n5SLs8nDCr4qwhLR8iOH2sDGzuxMqi_4b1MaaXTbYnFF7URqDb0CffnCCTOogA2QvERTzaMJubZzbkTlU5Ln4WwrR_F6HvJQ-0N1SJXvf9djO7m50OGIgdEU0VwNbcKCTfY5sQiva-8BBY4MIp3FjMCGn2uRlMoIOW3jsul5Rp0LK5I5ZoEcKvtHLYsoXTbTAJS_cBZCkdtUsp_cXLQf1UHUSvHhErwlPfzSjEc3cl6NCRVhkgSY1B1331f1P8qEq_EOl4l5UDpjGjdc_6RcqFh6dyWfMkkyeQI1pYr86SIEs8qZI1qAel8QYMWWhZ4gJ76MGwvsDeATelCBow_K1Qvo8xp1jTC0YsMKQN_DF5s6JpiF0WhonehkDo5SEiNcbPbYinPnmNGk4NxfTw9TNPwyywtReYdU7XB8GuKDM66-R9iRGx_oXw-UbGVasXbr2pN62J-kkRqhe6l3fSaYnXBnXz4AgLM33Z7S9giksHky2CI0C1dhtUSsFaR_u5lrDD8dBp3WWuTAWsd7cGPEnnP3N8g.PRPMeYH_Q_HAIwoeGr3HiQ/corelation.png" >}}

```python
# create a line plot with Plotly Express
fig = px.line(data, x='datetime', y='Tweet', title='Tweets Over Time', template='plotly_dark')

# display the plot
fig.show()
fig.write_image('Tweet Over Time.png')
```

{{< figure src="https://www.kaggleusercontent.com/kf/138873655/eyJhbGciOiJkaXIiLCJlbmMiOiJBMTI4Q0JDLUhTMjU2In0..yiBAE5Pi9vPgJibDALteuA.Yn89IYVAPjStn41ZIwDvRTs029BoO_ULM0adp7FnGJwFxdoUsrAp3K6HArgZ13rs9D3D8P7lUbyf2Gvr3t6OCKg2tAmfNcA5vvzzyNMlWMQyIzGGsKtw5nvuztUCa1coK0sK4CGC-8PyAsPLBIyEOhBKN76PeZvYwMgbZSIont073Kff-VR-c0_FHD1TzLA_N_4XBdZtC--9I1o91o-OpJ2cNI6CwTsQDDR3jPSlBrMmOrzBnVPRz7dtixFWp4Onvd3evk7B0AByO6rVDuIJ53JxcHhUU2Jx637TEADU2s1oyuUAZeYpcM9mn8vJQlprDO7fxfmWWtKLk7rOzkYSPedTNHPRv0UVUQfyzB_C3N41AVBLqqzePxqOkQ7atSIubYIi7Uw57AWMUb2nMR-nYlnxzY0TlJjYLoTmGopoUJuny9DO1oiCwE7dVPJTfWV7wKTtIJ3TZsa6rb0FT8Vu1gId2Cyf8Rqr6REPaUTCeTBfYNYWSFTaAT8KjYkVbweCfxytc9ZBnQ_mzq93UAHUOTxOd_uL1gmvV83qZIqaasZDYwWJ_EJ5BZy6shLyjcdtZ46zuMwJSFq4NquyBnkDLpssH9A9E0n64EsgMz8Wa7OmHcB3uzfWtdmPopyMZ-ARJuD_fcr6lOGB7cPinZhDVQVDoyEZ8VmPkx0nbrw-V0_hbY-SkC9Haua9Hr3j7jy4ystaDaArHKdAES0gih41oA.sCmyYx0ih0uUvMvMe7ksSA/Tweet%20Over%20Time.png" >}}

```python
df_copy = data.copy()

def get_multi_line_title(title, subtitle):
    return f"{title}<br><sub>{subtitle}</sub>"

title = get_multi_line_title(
    "Activity all over the day", 
    "BSI Data Leaked")

annual_counts = pd.DataFrame(df_copy['hour'].value_counts()).reset_index()
annual_counts.columns = ['hour', 'count']
annual_counts = annual_counts[annual_counts['hour'] != 2021].sort_values('hour')

fig = go.Figure(go.Bar(
    name="Annual Count", 
    x=annual_counts.hour, 
    y=annual_counts['count'], 
    marker_color=annual_counts['count'] 
))
# plt.xticks(range(1,25))
fig.update_layout(
    template='plotly_dark', 
    title=title, 
    xaxis = dict(
        tickvals=np.arange(0,24), 
        ticktext=[f"{h:02d}:00" for h in range(24)]
    )
)
fig.show()
fig.write_image('Number of Activity per hour period of the day.png')
```

{{< figure src="https://www.kaggleusercontent.com/kf/138873655/eyJhbGciOiJkaXIiLCJlbmMiOiJBMTI4Q0JDLUhTMjU2In0..8sHfNp9rW2zv4CG9eQTw3Q.O7gC4KlucAq5bcO1_gvA70JG4vf0MVKfbp_KTjO6sMfw9RjN6soMlUqQ5wzE_ROgJQ74mT-bEU1pBYkDzqfLATkCybHZIe6uDsLMFMgWSJThNPp90-9kRz9ualraGI_Tym06uabiLAgsIn-g5Vqu-f3128fM-JfNw7xubMyNB17oT57O-K7PAU3jaDcDf5qmxWdEeYPDWvPwsEFtTFTXiVvBXepyQHB2cdfpO6cDkvm4A7aoDsiwkctxYDIKW0N8vvtgYVOhU9U1273Cy1omSX8IX4nJr1eN0_31S0G7AhryNg-yxvL32JgnIKr7XddPrJ033g1XITMNPelVnFdIu1uRjPSQj_zbqbxPM0UohtFibtEqCBZVRUEc805A1LMtezAUrWDZSTL1-88HaFXqeGh333BnBLIiTI76LXdyjTrJYMyn8Wpk55LlVkE4UIu3tIQTGdr6NwtUgChreMQlN-BwRzvod_NUI8fR2mJBm1B6kVZDpDzTL4RGPznatd2WEwH8WEiNKYX7REzPrDGMICRh3MwgQ-B-lHa_oRUuwAZL-sjVNqOx0y4H4p9oetA-IVCkFmFMGf_z9OrVb1EusHyjUnBKG3NEsT60F4qfrGhxlxbb6dVU2xexAfFsrG1sGvHyrpjGSFU177LHDk3QlBdz_JVPFU6owF2BfR3-fTYQ4kwdYk3wpkrA3snnL5UNtJ5PHADD0FfAuxOzrk8YsA.FWJwIZTL7fdIXJYKgJ5jzA/Number%20of%20Activity%20per%20hour%20period%20of%20the%20day.png" >}}

```python
title = get_multi_line_title("Time of the day most Tweeted", "")

def format_hour(h: int):
    h = str(h)
    if len(h) == 1: 
        h = '0'+h
    h = h+ ":00"
    return h

oc = df_copy
hourly = oc.groupby('hour')['Tweet'].count()
hourly = pd.DataFrame(hourly).reset_index()
hourly.columns =['Hour of Day',"Number of Tweets"]
hourly['Hour of Day'] = hourly['Hour of Day'].apply(format_hour)

fig = px.line_polar(
    data_frame=hourly,
    r = 'Number of Tweets',
    theta='Hour of Day',
    line_close=True,
    color_discrete_sequence=['crimson'],
)

fig.update_layout(
    title=title, 
    template="plotly_dark",
    title_x=0.5,
    height=900,
#     width=900
)

fig.write_image('Time of day most tweeted.png')
fig.show()
```

{{< figure src="https://www.kaggleusercontent.com/kf/138873655/eyJhbGciOiJkaXIiLCJlbmMiOiJBMTI4Q0JDLUhTMjU2In0..MzRnoHxOjPv7bzdiXELSeQ.VdYu8ISsW-uRd4948K_D5s8l_E9xDeu6ccdNiPXg5KNXG4S39vWZNIkyK75qGkohfmuf_PtJbJ1Gzcju9zGmLuZJQUIfTXYFcFB58ahNDaLyZkgjYvL29R8WRDWp3nIcKOFqf3GIk6CZrm0T6nfGWymORKnwDJrRRJ8ZgM5PmSvQSv4kQgYSLH56Qp49S0uPZLUs_dZcy7OqQrVCDkPDCOZUBsaYXUB3zttB3GT-RUrEIeUXO3frrBsPCMA_VuBC4x_D9UfspKIJWf6lpDUpi61H_61yH6bgvnSyJN-fcLVgSroxGwvbRA_c8MNw7D5cG6pmfN5j0HdoJQ5TjPgGcVLgAtbhES22wxCL_NEmHE6W6S3w2jGtc1yNLiv8A_5giMgjsWvlHzIsXNg6l_EgYpgwsEhlDEOZKE8fvzyzTKRW25zHFufQvm-Sn1HGia8jrdkCYWcXu6nb25NNvirapwkZuPc_VYdStejtpEXi-vj2NDiezm4rJzlRRZRbnsmGFmPDHgl3vNNS5x76uFqSjdemfiug5PsAqpy_NPDQBWsBF95Bo_yZEHgyjrhczbAFMvoajHBM2B3friccnzyoC6oZi6wBYb7pR7VtuRAFmxNFlYPZjazoQnEpQGo6hgc3hDCAXRxtc7Gw_Ntr8rJ49_cZOnomEx8Tteq2AgkZYBx_tEyiSBICqFnBJIzST96NuGj4pfmHAfwAf2u7JZIHZQ.NsTmo1eGZ83rg0fome6-iQ/Time%20of%20day%20most%20tweeted.png" >}}

## Sentiment Analysis

```python
title = get_multi_line_title(
    'Sentiment Distribution',
    "What are the sentiments of Twitter Users about BSI Data Leaked?"
)

sentiment_pie = pd.DataFrame(df_copy['overall_sentiment'].value_counts() / df_copy.shape[0]*100).reset_index()
sentiment_pie.columns = ['Sentiment', 'Percentage']
fig = px.pie(sentiment_pie, values='Percentage', names='Sentiment', title=title)

fig.update_traces(marker=dict(colors=['#00adb5', '#f8b195', '#f67280']), textposition='inside')
fig.update_layout(
    title=title,
    title_x=0.48,
    template='plotly_dark',
    font=dict(
        family='Arial',
        size=20,  # Increase label font size to 20
        color='white'
    ),
    height=700,
    legend=dict(
        title='',
        orientation='h',
        yanchor='bottom',
        y=-0.15,
        xanchor='center',
        x=0.5,
        font=dict(
            family='Arial',
            size=12,
            color='white'
        )
    )
)
fig.write_image('Sentiment Distribution.png')
fig.show()
```
{{< figure src="https://www.kaggleusercontent.com/kf/138873655/eyJhbGciOiJkaXIiLCJlbmMiOiJBMTI4Q0JDLUhTMjU2In0..zc4V_I65N_xAP7j8istLyQ.JaMf0NsC4-v9DjWaOJWk27AWlOo4GlEfFawl_wYq_IlGXhi7tpgJZzZImCQFx3XGZ5Hbz_T1tGwEw0FLJNOwf0ySTf8btenjRNjOiMOeSzSXpVH7QsRq-2ZHU-o2mpdkujG2S5drkCyokQ1BsHl8LfUnV0DEkNuODZjqgbr4l0DihrnKQl-slFs0Et-GTcFbPqtMYHBkxOYw9gd5hu6BuNVppJmStgkekZRa1tqWcsdFgpS8VsRH8INoGdD7MKJcH-WFw4_0Aagt_nAasOhacd1L-fHXie6XQ2EZ8S-PI7Uh47puBZ3oGo_jNHmH-5_apkgUJwliKahdRu3IH_rnFJmpL8AzfpEAnOxH0J38PGNUJBZjlesgxjlg5yualMBhNyKcXp4_3eXAcCz3slwKFEXTEVa7A8UNDYta3CvrvWjSMVKRAmKkywD8XbPL0OUIt0B2KzsDxfObEPh6ogGKfKBwuD9I-JgMCGuj_4AYtUBjsCO-qH6gvCWJkY0SbGeihXPDV75HTp85qoj8R6IhrduPHNGmqY_Y6TXC1WhGqngxWLfAZJWY2AUVLFR2SmrvfvVRpJiKsbKDbuY6-ozjJCHZYmhYrd2Q-j4FkgBZOTRFJckS1lh5WXHARSD9ApyUwCKRz4qvm_QVBnPtSPoY0Z8aaGUF92i1nSAfb-UkMOVL9QBvcbAISRuZcp76LuiDS03LL2DkAnnVZ_3mS0NZyg.vZ3QKW6Ae3JubglEvJ3WKw/Sentiment%20Distribution.png" >}}

```python
sentiment_over_time = df_copy.sort_values('hour')[['hour', 'sentiment', 'overall_sentiment']]
annual_sentiment = pd.DataFrame(sentiment_over_time.groupby('hour')['overall_sentiment'].value_counts())
annual_sentiment.columns = ['Count']
annual_sentiment = annual_sentiment.reset_index()

title = get_multi_line_title('Hourly Tweet Sentiment', "Tweet Sentiments all over the day")
years = annual_sentiment.hour.unique().tolist()
sents = {'positive' : 'mediumseagreen', 'negative': 'crimson', 'neutral': 'royalblue'}

sentiment_bars = [] 
for s in sents.keys():
    current_year = annual_sentiment[annual_sentiment.overall_sentiment == s]
    sentiment_bars.append(
        go.Bar(name=s, x=current_year.hour, y=current_year.Count, marker_color=sents[s])
    )

fig = go.Figure(sentiment_bars)
fig.update_layout(
    template='plotly_dark',
    title=title,
    xaxis=dict(
        title='Time of Day',
        tickmode='array',
        tickvals=[0, 4, 8, 12, 16, 20, 24],
        ticktext=['12AM', '4AM', '8AM', '12PM', '4PM', '8PM', '12AM'],
        tickangle=0,
        showgrid=False,
        showline=True,
        linewidth=1,
        linecolor='white',
        mirror=True,
        gridcolor='white',
        zeroline=False,
    ),
    yaxis=dict(
        title='Count',
        showgrid=False,
        showline=True,
        linewidth=1,
        linecolor='white',
        mirror=True,
        gridcolor='white',
    ),
    legend=dict(
        title='',
        orientation='h',
        yanchor='bottom',
        y=-0.15,
        xanchor='center',
        x=0.5,
        font=dict(
            family='Arial',
            size=12,
            color='white'
        )
    )
)
fig.write_image('Hourly tweet sentiment.png')
fig.show()
```

{{< figure src="https://www.kaggleusercontent.com/kf/138873655/eyJhbGciOiJkaXIiLCJlbmMiOiJBMTI4Q0JDLUhTMjU2In0..j9OSyiwsvo41qaOQqt5zaQ.4q2kAy7Ahql8rXB840N3swsRG-e7Jcfg8Re-XttiHP4BtQ8s2XFkFS7DTLZBVZ_0yeiROzHd1yJq5D4zqgdpi2ARiTKN1pIJMfO5nijHn6TSL4lLBN2OSkF9OG4ZWeDLE3mxS3ry16VMS-2Hdfk1TxqV-QSA3XiJ2fii3ARnaKr6wrgeHMA5Z64nCKdPwzPJmPpaB2syymFgExXEGbYsjJ0SX0NuVc7uYZ5zg-71w-l-dUn_sq7clrYP_rsxjIIDmM7KYM93XU--p4SPJqKAkbMXIM55t4eQgHIfnukGCY5QTK-TkO56YtYOKqcGvl1VZPfHT7tLLYUrigwT-eiGQWNJrzvcNMTADvZJX1DCa7owaZOiZzPtbN7goRNKx6aW7sXOwG2P6Pa6FKGGBWLJ5jSFTFA6ADJQVjLf2pNss_HZdultideHuAe4PRgJ8mksYHH-gWVEMDlI1EYJmW5SLSSt9Vo4Kekbsp_dCuez4U_9zoGmEO37oEFQ0MkTzVyBV4PMkLN-cq2xmsY4TJodmdn5JX0ebDOBt_9ugQMr0avLPYZ76dSkqjItRX4Mm8h3EycKjYKuGNbUBPZIaigchUQA0FJiRnXQ2-jxTtRF3mitsFui9eM6UfxI2ARHVQyeU2pcu5KeQe6x7znd-HhJc1A3g2iMrsBmNNjSfHNqyZc_FQrc2vmKK_WaeDBVqr4CnYCjD-e0bT-o_fn4iWqhSw.8T19LF5annZzScbAbdcThg/Hourly%20tweet%20sentiment.png" >}}


```python
def flatten_list(l):
    return [x for y in l for x in y]

# color coding our wordclouds 
def red_color_func(word, font_size, position, orientation, random_state=None,**kwargs):
    return f"hsl(0, 100%, {random.randint(25, 75)}%)" 

def green_color_func(word, font_size, position, orientation, random_state=None,**kwargs):
    return f"hsl({random.randint(90, 150)}, 100%, 30%)" 

def yellow_color_func(word, font_size, position, orientation, random_state=None,**kwargs):
    return f"hsl(42, 100%, {random.randint(25, 50)}%)" 

def generate_word_clouds(neg_doc, neu_doc, pos_doc):
    # Display the generated image:
    fig, axes = plt.subplots(3, 1, figsize=(20,30))
    wordcloud_neg = WordCloud(max_font_size=50, max_words=100, background_color="black").generate(" ".join(neg_doc))
    axes[0].imshow(wordcloud_neg.recolor(color_func=red_color_func, random_state=3), interpolation='bilinear')
    axes[0].set_title("Negative Tweets", fontsize=30)
    axes[0].axis("off")

    wordcloud_neu = WordCloud(max_font_size=50, max_words=100, background_color="black").generate(" ".join(neu_doc))
    axes[1].imshow(wordcloud_neu.recolor(color_func=yellow_color_func, random_state=3), interpolation='bilinear')
    axes[1].set_title("Neutral Words", fontsize=30)
    axes[1].axis("off")
    
    wordcloud_pos = WordCloud(max_font_size=50, max_words=100, background_color="black").generate(" ".join(pos_doc))
    axes[2].imshow(wordcloud_pos.recolor(color_func=green_color_func, random_state=3), interpolation='bilinear')
    axes[2].set_title("Positive Words", fontsize=30)
    axes[2].axis("off")
    plt.savefig('wordcloud.png')

    plt.tight_layout()
    plt.subplots_adjust(wspace=1.5)
#     plt.style.use('dark_background')
    plt.show()


sentiment_sorted= data.sort_values('hour', ascending=False)
positive_top_100 = sentiment_sorted[sentiment_sorted['overall_sentiment'] == "positive"].iloc[:100]
negative_top_100 = sentiment_sorted[sentiment_sorted['overall_sentiment'] == "negative"].iloc[:100]
neutral_top_100 = sentiment_sorted[sentiment_sorted['overall_sentiment'] == "neutral"].iloc[:100]

cleanup = lambda x: [y for y in x.split() if y not in stopwords.words('english')]
neg_doc = flatten_list(negative_top_100['Tweet'].apply(cleanup))
pos_doc = flatten_list(positive_top_100['Tweet'].apply(cleanup))
neu_doc = flatten_list(neutral_top_100['Tweet'].apply(cleanup))

generate_word_clouds(neg_doc, neu_doc, pos_doc)
```

{{< figure src="https://www.kaggleusercontent.com/kf/138873655/eyJhbGciOiJkaXIiLCJlbmMiOiJBMTI4Q0JDLUhTMjU2In0..BAL5mtxHBTHJV_FYJrII_A.XloBDYOq-g5O-fKq92vUryMBHiROzlIp9vYybmQ5tquP0GodwcN2LFV6bjlmkH3_a6YXnDhIcfUJ9snAVHDv67Hul0k2wkFLjouA5BlhbaJ1FvJVov9CGuoP4Rg3rREe-bciFEnpBicfTr4AbFNbhB7G_zji0q6whlgsaWZSEZoxhLHyRREsfcSVanjobS29yK-CTTOWCMHnNPaFrnnyPGqLEuepmGLR_Vvh7PDyj3D7Hm26ThjXWvNE9zZ1kfQOGp4SXP4frVRg1NoSsr5_LPsEkPS1yLqZX6CyNt9Cnr4lSNrgknhYu2Jm-CuvyslZlmoiav3QNwlXCjlVvi9wMvhiHkd090TADfTqJU1fSzjwpIc5w8dfUMqSXLR7GueUX2enHjU6qdMxnSkfIX3tx0p6waUpUHf-pC9gIEAOQF2uxbs8Oe4uHjVoIs-jJgB4txxwpuMtCgVb8HagghD1DQsnnZlREFusxAdTkCCYYJjOYe-7eFf4JEoAEUSHx2-VwF4ne_QfTJwrrQapfqfpKsuy77s9RSFOQAXoODebfWsf9eqvfivTd4a4X-OYPi5oFVIBJwN2iN6xjoXz6ePkMbFHWMYE3Dsh8ZO750c7_KKufeYROkydRMRyqOGAwp1hMNIH9CIU0okBny5nWf1g1OvBnlIhh_CjB8be6GxPDAe3gre0XAMbFgUHu5LUYcvMnLgCkd3fryW_36rDO4XaZA.0q3yEM1o6BseFN2rCfyPMw/wordcloud.png" >}}

{{< css.inline >}}

<style>
.canon { background: white; width: 100%; height: auto; }
</style>

{{< /css.inline >}}
