+++
title = "About"
aliases = ["about-us", "about-hugo", "contact"]
author = "Dhifaf Ikhsan Sabil"
+++
I have a strong passion for the Technology Industry, and along with my experience in this field, I am highly motivated to continually learn and expand my expertise. With a growth mindset, I am dedicated to producing high-quality work, thriving on challenges, and actively collaborating to achieve excellence in every endeavor.